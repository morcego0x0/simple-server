#ifndef FSOBSERVER_H
#define FSOBSERVER_H

#include <QWidget>

namespace Ui {
class FSObserver;
}

class FSObserver : public QWidget
{
    Q_OBJECT

public:
    explicit FSObserver(QWidget *parent = 0);
    ~FSObserver();

private slots:
    void on_uploadFile_released();

    void on_downloadFile_released();

    void on_closeWindows_released();

private:
    Ui::FSObserver *ui;
};

#endif // FSOBSERVER_H
