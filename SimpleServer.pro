#-------------------------------------------------
#
# Project created by QtCreator 2016-03-06T15:36:36
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SimpleServer
TEMPLATE = app
DEFINES += ASIO_STANDALONE

SOURCES += main.cpp\
        mainwindow.cpp \
    applicationcontroller.cpp \
    server.cpp \
    tcpconnection.cpp \
    chatwindow.cpp \
    fsobserver.cpp \
    chat.cpp \
    chatviewcontroller.cpp

INCLUDEPATH += asio/include

LIBS += -lws2_32 -lwsock32

HEADERS  += mainwindow.h \
    applicationcontroller.h \
    server.h \
    tcpconnection.h \
    chatwindow.h \
    fsobserver.h \
    chat.h \
    chatviewcontroller.h

FORMS    += mainwindow.ui \
    chatwindow.ui \
    fsobserver.ui
