#include "chat.h"

Chat::Chat(TcpConnection::ConnectionPointer connection)
    : m_connection(connection)
{

}

void Chat::write_message(const QString &message)
{
    m_connection->WriteMessage(message);
}

void Chat::read_message()
{

}
