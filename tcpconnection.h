#ifndef TCPCONNECTION_H
#define TCPCONNECTION_H

#include "asio.hpp"
#include <memory>
#include <vector>
#include <QString>

class TcpConnection: public std::enable_shared_from_this<TcpConnection> {
public:
  typedef std::shared_ptr<TcpConnection> ConnectionPointer ;

  static ConnectionPointer Create(asio::io_service& io_service);

  asio::ip::tcp::socket& socket();

  void Start();

  void WriteMessage(const QString& data);
  void ReadMessage();

private:
  TcpConnection(asio::io_service& io_service);

  void handle_write(const asio::error_code& /*error*/, size_t /*bytes_transferred*/);

  asio::ip::tcp::socket socket_;
  std::string message_;
  std::vector<char> data;
};

#endif // TCPCONNECTION_H
