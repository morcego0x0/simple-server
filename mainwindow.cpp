#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtConcurrent/QtConcurrentRun>
#include "applicationcontroller.h"
#include "chatwindow.h"
#include "fsobserver.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    app_controller(new ApplicationController(this)),
    chat_window(new ChatWindow(this)),
    fs_observer(new FSObserver())
{
    ui->setupUi(this);
    app_controller->setView(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::newConnection(const QString &data)
{
    ui->connectionsList->addItem(data);
}

void MainWindow::on_connectToSelected_released()
{

}

void MainWindow::on_startServer_released()
{
    QtConcurrent::run(app_controller, &ApplicationController::startServer);
}

void MainWindow::on_StartChat_released()
{
    chat_window->show();
    emit startChat();
}

void MainWindow::on_fsNavigator_released()
{
    fs_observer->show();
}
