#include "applicationcontroller.h"

#include "asio.hpp"
#include "server.h"

#include "mainwindow.h"

ApplicationController::ApplicationController(QObject *parent)
    : QObject(parent)
{}

void ApplicationController::setView(MainWindow *view)
{
    m_view = view;
}

Server *ApplicationController::server()
{
    return m_server;
}

void ApplicationController::startServer()
{
    asio::io_service io_service;
    m_server = new Server(io_service);

    connect(m_server, &Server::newConnection, m_view, &MainWindow::newConnection);
    connect(m_view, &MainWindow::startChat, m_server, &Server::startChat);

    io_service.run();
}
