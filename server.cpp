#include "server.h"
#include <functional>
using asio::ip::tcp;

Server::Server(asio::io_service& io_service)
    : acceptor_(io_service, tcp::endpoint(tcp::v4(), 21)) {
    accept();
}

void Server::startChat()
{

}

void Server::accept()
{
    TcpConnection::ConnectionPointer connection = TcpConnection::Create(acceptor_.get_io_service());
    acceptor_.async_accept(connection->socket(),
    [this, connection](const asio::error_code& err) -> void {
        this->connections.push_back(connection);
        this->accept();
        emit newConnection(QString::fromStdString(connection->socket().remote_endpoint().address().to_string()));
    });
}
