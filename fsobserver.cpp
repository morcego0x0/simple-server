#include "fsobserver.h"
#include "ui_fsobserver.h"

FSObserver::FSObserver(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FSObserver)
{
    this->setHidden(true);
    ui->setupUi(this);
}

FSObserver::~FSObserver()
{
    delete ui;
}

void FSObserver::on_uploadFile_released()
{

}

void FSObserver::on_downloadFile_released()
{

}

void FSObserver::on_closeWindows_released()
{
    this->close();
}
