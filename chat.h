#ifndef CHAT_H
#define CHAT_H

#include "tcpconnection.h"
#include <QString>

class Chat
{
public:
    Chat(TcpConnection::ConnectionPointer connection);
private:
    void write_message(const QString& message);
    void read_message();
    TcpConnection::ConnectionPointer m_connection;
};

#endif // CHAT_H
