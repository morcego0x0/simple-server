#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}
class ApplicationController;
class ChatWindow;
class FSObserver;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void startChat();
public slots:
    void newConnection(const QString& data);
private slots:
    void on_connectToSelected_released();

    void on_startServer_released();

    void on_StartChat_released();

    void on_fsNavigator_released();

private:
    Ui::MainWindow *ui;
    ApplicationController* app_controller;
    ChatWindow* chat_window;
    FSObserver* fs_observer;
};

#endif // MAINWINDOW_H
