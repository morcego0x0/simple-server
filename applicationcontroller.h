#ifndef APPLICATIONCONTROLLER_H
#define APPLICATIONCONTROLLER_H

#include <QObject>

class Server;
class MainWindow;

class ApplicationController : public QObject
{
    Q_OBJECT
public:
    explicit ApplicationController(QObject *parent = 0);
    void startServer();

    void setView(MainWindow* view);

    Server* server();
signals:

public slots:
private:
    Server* m_server;
    MainWindow* m_view;
};

#endif // APPLICATIONCONTROLLER_H
