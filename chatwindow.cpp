#include "chatwindow.h"
#include "ui_chatwindow.h"
#include "chatviewcontroller.h"
ChatWindow::ChatWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChatWindow),
    m_controller(new ChatViewController())
{
    this->setHidden(true);
    ui->setupUi(this);

}

ChatWindow::~ChatWindow()
{
    delete ui;
}

void ChatWindow::PutMessage(const QString &message)
{
    ui->chatWindow->append("\n" + message);
}

void ChatWindow::on_sendMessage_released()
{
  QString str = ui->chatWindow->toPlainText();
  emit sendMessage(str);
}

void ChatWindow::on_closeButton_released()
{
    this->close();
}
