#include "tcpconnection.h"

#include <functional>
#include <memory>

TcpConnection::TcpConnection(asio::io_service &io_service)
    : socket_(io_service)
{
}

TcpConnection::ConnectionPointer
TcpConnection::Create(asio::io_service &io_service)
{
    return ConnectionPointer(new TcpConnection(io_service));
}

asio::ip::tcp::socket &TcpConnection::socket()
{
    return socket_;
}

void TcpConnection::Start()
{
}

void TcpConnection::WriteMessage(const QString &data)
{
    asio::async_write(socket_, asio::buffer(data.toStdString()),
                      std::bind(&TcpConnection::handle_write, this,
                                  std::error_code(),
                                100));

    ReadMessage();

}

void TcpConnection::ReadMessage()
{
    auto self(shared_from_this());
    asio::async_read(socket_,
        asio::buffer(&data[0], 1000),
        [this, self](std::error_code ec, std::size_t /*length*/) {
        self->ReadMessage();
    });
}

void TcpConnection::handle_write(const asio::error_code &, size_t) {
}
