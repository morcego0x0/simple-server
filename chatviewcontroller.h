#ifndef CHATVIEWCONTROLLER_H
#define CHATVIEWCONTROLLER_H

#include <QObject>

class Server;
class ChatWindow;

class ChatViewController: public QObject
{
   Q_OBJECT
public:
    ChatViewController();
    void setView(ChatWindow *view);
    void setServer(Server* server);
private:
    Server* m_server;
    ChatWindow* m_chat;
};

#endif // CHATVIEWCONTROLLER_H
