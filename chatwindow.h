#ifndef CHATWINDOW_H
#define CHATWINDOW_H

#include <QDialog>

namespace Ui {
class ChatWindow;
}

class ChatViewController;

class ChatWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ChatWindow(QWidget *parent = 0);
    ~ChatWindow();
    void PutMessage(const QString& message);
signals:
    void sendMessage(const QString& message);
private slots:
    void on_sendMessage_released();

    void on_closeButton_released();

    private:
    Ui::ChatWindow *ui;
    ChatViewController* m_controller;
};

#endif // CHATWINDOW_H
