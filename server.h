#ifndef SERVER_H
#define SERVER_H

#include "tcpconnection.h"

#include <QObject>
#include <vector>

#include "asio.hpp"

class Server: public QObject
{
    Q_OBJECT
public:
    Server(asio::io_service &io_service);

signals:
    void newConnection(const QString& str);
public slots:
    void startChat();
private:
    void accept();

    asio::ip::tcp::acceptor acceptor_;
    std::vector<TcpConnection::ConnectionPointer> connections;
};

#endif // SERVER_H
